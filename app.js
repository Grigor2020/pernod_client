var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var middleware = require('./middleware')

const request = require('request');
var statics = require('./static');

var cors = require('cors')
var http = require('http').Server(app);

var indexRouter = require('./routes/index');
var authRouter = require('./routes/auth/auth');
var resetPasswordRouter = require('./routes/auth/resetPassword');
var usersRouter = require('./routes/users');
var loginRouter = require('./routes/auth/signIn');
var regRouter = require('./routes/auth/signUp');
var cartRouter = require('./routes/cart/cart');
var productRouter = require('./routes/product/product');
var catalogRouter = require('./routes/catalog/catalog');
var brandRouter = require('./routes/brand/brand');
var checkoutRouter = require('./routes/checkout/checkout');
var profileRouter = require('./routes/profile/profile');
var newsRouter = require('./routes/news/news');
var favoriteRouter = require('./routes/favorite/favorite');
var contactRouter = require('./routes/contact/contact');
var ordersRouter = require('./routes/orders/orders');
var searchRouter = require('./routes/search/search');
var historyRouter = require('./routes/history/history');
var servicePageRouter = require('./routes/servicePages/servicePages');
var specialOffersPageRouter = require('./routes/catalog/specialOffers');
var testRouter = require('./routes/testRouter');
var aboutRouter = require('./routes/about/about');
var ourValuesRouter = require('./routes/ourValues/ourValues');
var socialResponseibilityRouter = require('./routes/socialResponseibility/socialResponseibility');
var passRecoveryRouter = require('./routes/auth/resetPassword');
var verifyEmailRouter = require('./routes/auth/verifyEmail');
var paymentAndDeliveryRouterRouter = require('./routes/paymentAndDelivery/paymentAndDelivery');

var app = express();



app.use(cors())
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.enable('trust proxy')
app.use(function (req, res, next) {
    req.siteData = {};
    req.siteData.seoInstruments = {
        title : "Pernod Ricard",
        url : req.protocol + '://' + req.get('host') + req.originalUrl,
        description : "Description",
        type : "article",
        image : "/images/site/share_banner.png"
    };
    res.setHeader("Access-Control-Allow-Headers", "x-access-token,void,authorization");
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", true);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT'); // OPTIONS, PATCH,
    res.setHeader('Cache-Control', 'no-cache'); // OPTIONS, PATCH,
    res.setHeader('X-Frame-Options', 'deny'); // OPTIONS, PATCH,
    res.removeHeader('Server');
    res.removeHeader('X-Powered-By');
    next()
})



app.use(function (req, res, next) {middleware.setGuest(req, res, next)});
app.use(function (req, res, next) {middleware.getAllLanguage(req, res, next)});
app.use(function (req, res, next) {middleware.setLanguage(req, res, next)});
app.use(function (req, res, next) {middleware.getCountries(req, res, next)});

app.use(function (req, res, next) {middleware.checkUser(req, res, next)});
app.use(function (req, res, next) {middleware.getPopularProducts(req, res, next)});
app.use(function (req, res, next) {middleware.getCategoriesWithBrands(req, res, next)});
app.use(function (req, res, next) {middleware.getLoginSlide(req, res, next)});
app.use(function (req, res, next) {middleware.getFooterInfo(req, res, next)});
app.use(function (req, res, next) {middleware.getLastNews(req, res, next)});

app.use(function(req, res, next) {
    var pathname = req.originalUrl;
    var pathName = pathname.split('/');
    var pathS = pathName[1].split('?');
    app.locals.pathName = pathS.length > 1 ? pathS[0] : pathName[1]
    app.locals.permition = '0';
    var permition = req.cookies['permition'];
    if(typeof permition !== 'undefined'){
        app.locals.permition = '1'
    }
    next();
});

app.use(function (req, res, next) {
    let siteLang = '2';
    if(typeof req.siteData.lang !== "undefined")
        siteLang = req.siteData.lang
    var options = {
        url: statics.API_URL + '/v1/translate/all/'+siteLang,
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH,

        }
    }
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        if(!info.error){
            // req.siteData.translates = info.data.translates;
            app.locals.translations = info.data.translates;
            next();
        }
    });
});


// app.use(function (req, res, next) {
//     if (req.ip == "178.160.251.193" || req.ip == "37.186.118.202") {
//         next()
//     }else{
//         res.render('default/coming_soon');
//         res.end();
//     }
//     // res.render('default/coming_soon');
//     // res.end();
// })


app.use('/', testRouter);
app.use('/test', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/reset-password', resetPasswordRouter);

app.use('/login', loginRouter);
app.use('/sign-up', regRouter);
app.use('/cart', cartRouter);
app.use('/product', productRouter);
app.use('/catalog', catalogRouter);
app.use('/brand', brandRouter);
app.use('/checkout', checkoutRouter);
app.use('/profile', profileRouter);
app.use('/news', newsRouter);
app.use('/favorite', favoriteRouter);
app.use('/contact', contactRouter);
app.use('/orders', ordersRouter);
app.use('/search', searchRouter);
app.use('/history', historyRouter);
app.use('/about', aboutRouter);
app.use('/service-page', servicePageRouter);
app.use('/special-offers', specialOffersPageRouter);
app.use('/our-values', ourValuesRouter);
app.use('/social-responsibility', socialResponseibilityRouter);
app.use('/password-recovery', passRecoveryRouter);
app.use('/verify-email', verifyEmailRouter);
app.use('/payment-and-delivery', paymentAndDeliveryRouterRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.render('default/not_found',{siteData :req.siteData});
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    // res.render('error');
});



module.exports = app;
