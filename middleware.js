const request = require('request');
var statics = require('./static');
var staticMethods = require('./model/staticMethods');

const checkUser = function (req, res, next) {
    var userCookie = req.cookies['void'];
    // var guestCookie = req.cookies['guest'];
    // var userCookie = req.siteData.userInfo;
    //console.log('userCookie',userCookie)
    var guestCookie = req.siteData.guestInfo;
    var options = {
        url: statics.API_URL + '/v1/auth/check-user',
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH,
        },
        form: {
            langId: req.siteData.lang
        }
    }

    if (typeof userCookie !== "undefined") {
        options.headers.void = userCookie
    }
    if (typeof guestCookie !== "undefined") {
        options.headers.guest = guestCookie
    }
    request.post(
        options
        , function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            // console.log('checkUser jsonBody', jsonBody)
            if (!jsonBody.error) {
                req.siteData.favouriteCount = jsonBody.data.favouriteCount
                req.siteData.allProductQuantity = jsonBody.data.cart.allProdQuantity
                req.siteData.cart = jsonBody.data.cart
                if (typeof userCookie !== "undefined") {
                    req.siteData.userInfo = jsonBody.data.userInfo
                }
            } else {
                // setGuest(req, res, next)
                req.siteData.favouriteCount = 0
                req.siteData.allProductQuantity = 0
                req.siteData.cart = []
                res.clearCookie('void');
            }
            next();
        }
    )

}

const setGuest = function (req, res, next) {
    // console.log('req.siteData.guestInfo',)
    // var guestToken = req.siteData.guestInfo;
    var guestToken = req.cookies['guest'];
    if(typeof guestToken === 'undefined'){
        var opt = {
            url: statics.API_URL+'/v1/guest',
            headers: {
                'Accept': 'application/json',
                'authorization' : statics.API_AUTH
            }
        }
        request.post(
            opt
            , function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                // console.log('setGuest 1 jsonBody',jsonBody)
                if(!jsonBody.error){
                    res.cookie('guest', jsonBody.data.token,{expires: new Date(Date.now() + 900000000),path: '/'});
                    req.siteData.guestInfo = jsonBody.data.token;
                }
                next();
            }
        )
    }else{
        request.post(
            {
                url: statics.API_URL + '/v1/guest/checkValidGuest',
                headers: {
                    'Accept': 'application/json',
                    'authorization': statics.API_AUTH,

                },
                form: {
                    guest : guestToken
                }
            }
            , function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                // console.log('setGuest 2 jsonBody',jsonBody)
                if(!jsonBody.error){
                    if(jsonBody.msg == 'new-token'){
                        res.cookie('guest', jsonBody.data.token,{expires: new Date(Date.now() + 900000000),path: '/'});
                        req.siteData.guestInfo = jsonBody.data.token;
                        next();
                    }else{
                        req.siteData.guestInfo = guestToken;
                        next();
                    }
                }
            }
        )
    }
}

const getAllLanguage = function (req,res,next) {
    var options = {
        url: statics.API_URL + '/v1/language/all',
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH,

        }
    }
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        // console.log('getAllLanguage 2 info',info)
        if(!info.error){
            req.siteData.allLanguages = info.data.languages;
            next();
        }
    });
}

const setLanguage = function (req, res, next) {
    var cookieLang = req.cookies['lang'];
    if(typeof cookieLang === 'undefined'){
        for(var i = 0; i < req.siteData.allLanguages.length; i++){
            if(req.siteData.allLanguages[i].id == '2'){
                res.cookie('lang', req.siteData.allLanguages[i].id, {expire: 900000000 + Date.now()});
                req.siteData.lang = req.siteData.allLanguages[i].id;
            }
        }
    }else{
        // res.cookie('lang', '2',{expires: new Date(Date.now() + 900000000),path: '/'});
        req.siteData.lang = cookieLang;
    }
    next();
}

const getCountries = function (req, res, next) {
    var permition = req.cookies['permition'];
    if(typeof permition === 'undefined'){
        var options = {
            url: statics.API_URL + '/v1/country/all',
            headers: {
                'Accept': 'application/json',
                'authorization': statics.API_AUTH,

            }
        }
        request(options, function (error, response, body) {
            const info = JSON.parse(body);
            // console.log('getCountries 2 info',info)
            if(!info.error){
                req.siteData.years = [];
                for(var i = 2020; i > 1940 ; i--){
                    req.siteData.years.push(i)
                }

                req.siteData.monts = [
                    {name:'JAN',val:'0'},
                    {name:'FEB',val:'1'},
                    {name:'MAR',val:'2'},
                    {name:'APR',val:'3'},
                    {name:'MAY',val:'4'},
                    {name:'JUN',val:'5'},
                    {name:'JUL',val:'6'},
                    {name:'AUG',val:'7'},
                    {name:'SEP',val:'8'},
                    {name:'OCT',val:'9'},
                    {name:'NOV',val:'10'},
                    {name:'DEC',val:'11'}
                ];

                req.siteData.days = [];
                for(var j = 1; j < 32; j++){
                    req.siteData.days.push(j)
                }
                req.siteData.countries = info.data;
                next();
            }
        });
    }else {
        next();
    }

}

const getCategoriesWithBrands = function (req,res,next) {
    let siteLang = '2';
    if(typeof req.siteData.lang !== "undefined")
        siteLang = req.siteData.lang
    var options = {
        url: statics.API_URL + '/v1/categories-brands/all/'+siteLang,
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH,

        }
    }
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
         // console.log('getCategoriesWithBrands 2 info',info)
        if(!info.error){

            req.siteData.categories = info.data.categoriesWithBrands;
            req.siteData.brands = info.data.brands;
            // req.siteData.litrages = info.data.litrages;
            req.siteData.litrages = staticMethods.litrages()
            next();
        }
    });
}

const getPopularProducts = function (req,res,next) {
    let siteLang = '2';
    if(typeof req.siteData.lang !== "undefined")
        siteLang = req.siteData.lang
    var options = {
        url: statics.API_URL + '/v1/product/get-popular/'+siteLang,
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH,

        }
    }
    if(typeof req.siteData.userInfo !== "undefined" ){
        options.headers.void = req.siteData.userInfo.token
    }
    if(typeof req.siteData.guestInfo !== "undefined" ){
        options.headers.guest = req.siteData.guestInfo
    }
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        if(!info.error){
            req.siteData.popularProducts = info.data.popularProducts;
            req.siteData.ssProduct = info.data.ssProduct;
            next();
        }
    });
}

const getLoginSlide = function (req,res,next) {
    let siteLang = '2';
    if(typeof req.siteData.lang !== "undefined")
        siteLang = req.siteData.lang
    var options = {
        url: statics.API_URL + '/v1/slide/get-login-slide/'+siteLang,
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH,
        }
    }
    // console.log('getLoginSlide options',options)
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        // console.log(':::::::::::::::::::info', info)
        if(!info.error){
            req.siteData.loginSlide = info.data.login_slide;
            next();
        }else{
            req.siteData.loginSlide = [];
        }
    });
}

const getLastNews = function (req,res,next) {
    let siteLang = '2';
    if(typeof req.siteData.lang !== "undefined")
        siteLang = req.siteData.lang
    var options = {
        url: statics.API_URL + '/v1/news/get-latest/'+siteLang,
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH,
        }
    }
    // console.log('getLastNews options',options)
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        if(!info.error){
            req.siteData.latestNews = info.data;
            next();
        }else{
            req.siteData.latestNews = [];
        }
    });
}

const getFooterInfo = function (req,res,next) {
    var options = {
        url: statics.API_URL + '/v1/settings',
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH,
        }
    }
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        if(!info.error){
            req.siteData.settings = info.data;
            next();
        }else{
            req.siteData.settings = [];
        }
    });
}


// const getTranslations = function (req,res,next) {
//
// }
module.exports.getCountries = getCountries;
module.exports.checkUser = checkUser;
module.exports.setGuest = setGuest;
module.exports.getAllLanguage = getAllLanguage;
// module.exports.getTranslations = getTranslations;
module.exports.getCategoriesWithBrands = getCategoriesWithBrands;
module.exports.setLanguage = setLanguage;
module.exports.getPopularProducts = getPopularProducts;
module.exports.getLoginSlide = getLoginSlide;
module.exports.getFooterInfo = getFooterInfo;
module.exports.getLastNews = getLastNews;
