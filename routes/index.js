var express = require('express');
var router = express.Router();
var statics = require('../static');
const request = require('request');
var staticMethods = require('../model/staticMethods');
var CronJob = require('cron').CronJob;

// var job = new CronJob('* * * * * *', function() {
//     console.log('You will see this message every second');
// }, null, true, 'America/Los_Angeles');
// job.start();

router.get('/', function (req, res, next) {
    Promise.all([
        getSlider(req),
    ])
        .then(function ([slider]) {
            res.render('index',
                {
                    siteData: req.siteData,
                    slide: slider,
                });
        })
});

module.exports = router;


function getSlider(req) {
    return new Promise((resolve, reject) => {
        let siteLang = '2';
        if(typeof req.siteData.lang !== "undefined")
            siteLang = req.siteData.lang
        var options = {
            url: statics.API_URL + '/v1/slide/all/'+siteLang,
            headers: {
                'Accept': 'application/json',
                'authorization': statics.API_AUTH,
            }
        }
        request(options, function (error, response, body) {
            const info = JSON.parse(body);

            if(!info.error){
                resolve(info.data.slide)
            }else{
                resolve([])
            }

        });
    })

}
