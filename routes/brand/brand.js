var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');



router.get('/:brand', function (req, res, next) {

    console.log('........................................brandId',req.params.brand);
    staticMethods.sendApiPost(req,'/v1/brand/getOneBrand',{brand : req.params.brand,langId : req.siteData.lang}, function (data) {
        console.log('data',data)
        if(!data.error){
            res.render('brand/brand',
                {
                    brand : data.data.brand,
                    products : data.data.products,
                    siteData: req.siteData
                });
        }
    })
});

router.post('/getOneBrand', function (req, res, next) {

    staticMethods.sendApiPost(req,'/v1/brand/getOneBrand',{brand : req.query.brand}, function (data) {
        res.render('brand/brand',
            {
                siteData: req.siteData
            });
    })

});

module.exports = router;
