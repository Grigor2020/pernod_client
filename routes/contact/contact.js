var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');

router.get('/', function (req, res, next) {
    res.render('contact/contact',
        {
            siteData: req.siteData
        });
});

router.post('/send-msg', function (req, res, next) {
    console.log('req.body',req.body)
    if
    (
        req.body.name !== "" &&
        req.body.email !== "" &&
        req.body.subject !== "" &&
        req.body.message !== ""
    ){
        staticMethods.sendApiPost(req,'/v1/contact/msg',req.body, function (data) {
            if(!data.error){
                res.json(data)
            }
        })
    }else {
        res.json({error:true})
    }
});

module.exports = router;
