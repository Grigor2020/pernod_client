var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');


router.get('/', function (req, res, next) {
    let slug = req.query.id;
    console.log('ppppppppppp')
    staticMethods.sendApiPost(req,'/v1/product/get-one-product',{slug : slug,langId: req.siteData.lang}, (data)=>{
        if(!data.error){

            req.siteData.seoInstruments.title = data.data.product.seo_title
            req.siteData.seoInstruments.description = data.data.product.seo_desc
            req.siteData.seoInstruments.image = data.data.product.image
            // req.siteData.seoInstruments.url = statics.MAIN_URL+'/'+
            console.log('data.data.product',data.data.product)
            res.render('product/product',
                {
                    product:data.data.product,
                    relativeProduct:data.data.relativeProduct,
                    filters : data.data.staticFilters.concat(data.data.addFilters),
                    // addFilters:data.data.addFilters,
                    // staticFilters:data.data.staticFilters,
                    siteData: req.siteData
                });
        }else{
            res.json({error : true})
        }
    })
});


router.post('/search', function (req, res, next) {
    let vals = req.body.vals;
    // console.log('vals',vals)
    staticMethods.sendApiPost(req,'/v1/product/search',{vals : vals}, (data)=>{
          res.json({product:data.data})
    })
});

module.exports = router;
