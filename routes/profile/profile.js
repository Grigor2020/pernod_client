var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');
const PDFDocument = require('pdfkit');
const fs = require('fs');
let pdf = require("html-pdf");
let ejs = require("ejs");

router.get('/', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        staticMethods.sendApiPost(req, '/v1/profile/get-info', {langId: req.siteData.lang}, function (data) {
            console.log('data.orders',data.data.orders)
            if (!data.error) {
                var loyaltyInfo = JSON.stringify(data.data.loyalty)
                res.render('profile/profile',
                    {
                        siteData: req.siteData,
                        loyaltyInfo: loyaltyInfo,
                        data: data.data
                    });
            } else {
                res.redirect('/')
            }
        })
    }else{
        res.redirect('/login')
    }
});

router.get('/orders', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        staticMethods.sendApiPost(req,'/v1/profile/get-orders',{langId : req.siteData.lang},function (data) {
            // console.log('data.data',data.data)
            res.render('profile/orders',
                {
                    data: data.data,
                    siteData: req.siteData
                });
        })
    }else{
        res.redirect('/login')
    }
});

router.post('/orders/remove-order', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        staticMethods.sendApiPost(req,'/v1/profile/remove-order',{orderId : req.body.orderId},function (data) {
            res.json(data)
        })
    }else{
        res.redirect('/login')
    }
});
router.post('/change-password', function (req, res, next) {
    // console.log('/change-password req.body',req.body)
    if(typeof req.siteData.userInfo !== "undefined") {
        staticMethods.sendApiPost(req,'/v1/profile/change-password',{oldPAss : req.body.oldPAss,newPass : req.body.newPass,newRePass : req.body.newRePass},function (data) {
            res.json(data)
        })
    }else{
        res.redirect('/login')
    }
});
router.get('/orders/:id', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        staticMethods.sendApiPost(req,'/v1/profile/get-order-by-id',{langId : req.siteData.lang,id:req.params.id},function (data) {
            // console.log(' data.data', data)
            if(!data.error){
                res.render('profile/order_item',
                    {
                        siteData: req.siteData,
                        orderInfo : data.data.orderInfo,
                        orderProducts : data.data.orderProducts
                    });
            }else{
                res.render('default/not_found',
                    {
                        siteData: req.siteData
                    });
            }

        })
    }else{
        res.redirect('/login')
    }
});

router.get('/points', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        res.render('profile/points',
            {
                siteData: req.siteData
            });
    }else{
        res.redirect('/login')
    }
});

router.get('/personal-data', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        staticMethods.sendApiPost(req,'/v1/profile/get-user-addresses',{langId : req.siteData.lang},function (data) {
            res.render('profile/personal_data',
                {
                    data: data.data,
                    siteData: req.siteData
                });
        })
    }else{
        res.redirect('/login')
    }
});

router.post('/add-address', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        var sendData = {
            name : req.body.name,
            last_name : req.body.last_name,
            phone : req.body.phone,
            email : req.body.email,
            city_id : req.body.city_id,
            text : req.body.text
        }
        staticMethods.sendApiPost(req,'/v1/profile/address/add',sendData,function (data) {
            // console.log('............data',data)
            res.json({error:false,data:data.data})
        })
    }else{
        res.redirect('/login')
    }
});

router.post('/personal-data', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        var sendData = {
            name: req.body.name,
            last_name: req.body.last_name,
            phone: req.body.phone
        }
        staticMethods.sendApiPost(req,'/v1/profile/user-info/update',sendData,function (data) {
            res.json(data)
        })
    }else{
        res.redirect('/login')
    }
});

router.post('/set-defaults', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        staticMethods.sendApiPost(req,'/v1/profile/address/set-default',{id: req.body.id},function (data) {
            res.json(data)
        })
    }else{
        res.redirect('/login')
    }
});

router.post('/address/remove', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        staticMethods.sendApiPost(req,'/v1/profile/address/remove',{id: req.body.id},function (data) {
            res.json(data)
        })
    }else{
        res.redirect('/login')
    }
});
router.post('/address/edit', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        var body = {
            address_id : req.body.address_id,
            name : req.body.name,
            last_name : req.body.last_name,
            phone : req.body.phone,
            email : req.body.email,
            city_id : req.body.city_id,
            text : req.body.text,
        }
        staticMethods.sendApiPost(req,'/v1/profile/address/edit',body,function (data) {
            res.json(data)
        })
    }else{
        res.redirect('/login')
    }
});
router.post('/change-email', function (req, res, next) {
    if(req.siteData.userInfo.email !== req.body.email){
        if(typeof req.siteData.userInfo !== "undefined") {
            staticMethods.sendApiPost(req,'/v1/profile/change-email',{email:req.body.email},function (data) {
                // res.clearCookie('void');
                res.json(data)
            })
        }else{
            res.redirect('/login')
        }
    }else{
        res.json({error:true, msg : 'same'})
    }
});


router.post('/remove-account', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        staticMethods.sendApiPost(req,'/v1/profile/remove-account',{},function (data) {
            res.clearCookie('void');
            res.json({error:false})
        })
    }else{
        res.redirect('/login')
    }
});

router.get('/order/get-invoice/:publicId', function (req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        // console.log('req.body.publicId',req.body.publicId)
        staticMethods.sendApiPost(req,'/v1/profile/get-order-by-id',{langId : req.siteData.lang,id:req.params.publicId},function (data) {
            if(!data.error){
                let logo = statics.MAIN_URL+'/images/content/general/Logo.png';
                var grantTotal = 0;
                var litrages = staticMethods.litrages()
                for(var i = 0; i < data.data.orderProducts.length; i++){
                    grantTotal+= parseInt(data.data.orderProducts[i].quantity) * parseInt(data.data.orderProducts[i].price.replace(/\s/g, ''));
                    data.data.orderProducts[i].subTotal = staticMethods.formatMoney(parseInt(data.data.orderProducts[i].quantity) * parseInt(data.data.orderProducts[i].price.replace(/\s/g, '')))
                    litrages.forEach(function (litrage) {
                        if(litrage.id == data.data.orderProducts[i].litrage_id){
                            data.data.orderProducts[i].litrage = litrage.name
                        }
                    })
                }

                ejs.renderFile('views/profile/invoice-template.ejs',
                    {
                        products: data.data.orderProducts,
                        logo:logo,orderInfo : data.data.orderInfo,
                        userInfo : req.siteData.userInfo,
                        grantTotal : staticMethods.formatMoney(grantTotal)
                    }, (err, data) => {
                    if (err) {
                            res.send(err);
                        } else {
                            pdf.create(data, {}).toFile('./public/pdf/'+req.params.publicId+'.pdf', function (err, data) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    res.setHeader('Content-type', 'application/pdf');
                                    res.download('./public/pdf/'+req.params.publicId+'.pdf');
                                }
                            });
                        }
                });
            }else{
                console.log(' data.data', data)
            }
        })
    }else{
        res.json({error:true})
    }
});

function header(){
    return `<div style="font-size:7px;white-space:nowrap;margin-left:38px;">
                        ${new Date().toDateString()}
                        <span style="margin-left: 10px;">Generated PDF</span>
                    </div>`;
}
module.exports = router;
