var express = require('express');
var router = express.Router();
var statics = require('../../static');
var staticMethods = require('../../model/staticMethods');



router.get('/', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/favorite/all',{langId:req.siteData.lang}, function (data) {
        // console.log('data...', data)
        // console.log('data', data.data.favourite)
        if(!data.error){
            res.render('favorite/favorite',
                {
                    favourite : data.data.favourite,
                    siteData: req.siteData
                });
        }else {
            res.redirect('/')
        }

    })

});

router.post('/add', function (req, res, next) {
    // console.log('req.body',req.body)
    staticMethods.sendApiPost(req,'/v1/favorite/add',{productId:req.body.productId}, function (data) {
        // console.log('data',data)
        res.json(data)
    })
});

module.exports = router;
