var express = require('express');
var router = express.Router();
var staticMethods = require('../../model/staticMethods');
var statics = require('../../static');
var request = require('request');

router.get('/create', function (req, res, next) {
    console.log('...........................................................................req.query',req.query);

    // var body = {
    //     name : 'Grigor',
    //     last_name: 'testlASTNAME',
    //     email: 'sadoyangrigor@gmail.com',
    //     phone: '21312312',
    //     cities_id: 6553,
    //     address: 'Shinararner 15',
    //     delivery_date: '2021-01-19',
    //     delivery_time: '14:00 - 16:00',
    //     comment: 'sa dsad sadas',
    //     payment_type: 3,
    //     send_email: 1,
    //     private: 0,
    //     save_to_my: 1,
    //     address_id: null
    // }

    var body = {
        name : req.query.name,
        last_name : req.query.last_name,
        email : req.query.email,
        phone : req.query.phone,
        cities_id : req.query.cities_id,
        address : req.query.address,
        delivery_date : req.query.delivery_date,
        delivery_time : req.query.delivery_time,
        payment_type : req.query.payment_type,
        send_email : req.query.send_email,
        private : req.query.private,
        save_to_my : req.query.save_to_my,
        comment: req.query.comment,
        subscribing: req.query.subscribing,
        address_id : req.query.address_id
    }

    console.log('...........................................................................body',body);
    staticMethods.sendApiPost(req,'/v1/orders/create', body, function (data) {
        console.log('............asd..................................................................',data)
        if(!data.error){
            if(data.data.type == 1){
                var order_payment_id = data.data.order_payment_id
                var Amount = data.data.Amount
                console.log('OrderID',order_payment_id)
                console.log('Amount',Amount)
                var options = {
                    url: 'https://servicestest.ameriabank.am/VPOS/api/VPOS/InitPayment',
                    headers: {
                        'Accept': 'application/json',
                    },
                    form : {
                        "ClientID": "a551a6a0-9957-4e0c-932d-c914e85278f3",
                        "Amount": Amount,
                        "OrderID": order_payment_id,
                        "BackURL": statics.MAIN_URL+'/orders/payment-return',
                        "Username": "3d19541048",
                        "Password": "lazY2k",
                        "Description": "Test Descrption"
                    }
                }
                console.log('options',options)
                request.post(
                    options
                    , function (err, httpResponse, body) {
                        var jsonBody = JSON.parse(body);
                        console.log('InitPayment  CCC',jsonBody)
                        var opts =
                            {
                                paymentInitResponse : JSON.stringify(jsonBody),
                                order_payment_id : order_payment_id,
                                PaymentID : jsonBody.PaymentID
                            };
                        console.log('opts',opts)
                        staticMethods.sendApiPost(req,'/v1/orders/add-payment-init-response',
                            opts, function (data) {
                                if(jsonBody.ResponseCode === 1  && jsonBody.ResponseMessage === 'OK'){
                                    console.log('777777777jsonBody',jsonBody)
                                    var url = "https://servicestest.ameriabank.am/VPOS/Payments/Pay?id="+jsonBody.PaymentID+"&lang=am";
                                    console.log('url',url)
                                    res.redirect(url)
                                }else{
                                    res.render('orders/failed',{
                                        siteData: req.siteData
                                    })
                                }
                            })
                    }
                )
            }else if (data.data.type == 3 || data.data.type == 4 ){
                staticMethods.sendApiPost(req,'/v1/orders/add-payment-details',{paymentDetails : null,OrderID : data.data.order_payment_id, paymentStatus : 1}, function (data) {
                    console.log('TYPE 3 data',data)
                    if(!data.error){
                        console.log('......data.............data.....data.......', data)
                        // public_id
                        res.redirect('/orders/success/'+data.data.public_id)
                        // res.render('orders/cash_success_page',{
                        //     siteData: req.siteData,
                        //     orderProducts:data.data.products,
                        //     public_id:data.data.public_id
                        // })
                    }else{
                        res.redirect('/orders/failed')
                    }
                });
            }else {
                res.redirect('/orders/failed')
            }
        }else {
            res.redirect('/orders/failed')
        }
    })
});

router.get('/payment-return', function (req, res, next) {
    console.log('kkkkkkkkkkkkkkkkkkkkkk req.query',req.query)
    var orderID = req.query.orderID;
    var resposneCode = req.query.resposneCode;
    var PaymentID = req.query.paymentID;
    var opaque = req.query.opaque;
    var description = req.query.description;
    if(resposneCode === '00'){
        var options = {
            url: 'https://servicestest.ameriabank.am/VPOS/api/VPOS/GetPaymentDetails',
            headers: {
                'Accept': 'application/json',
            },
            form : {
                "PaymentID": PaymentID,
                "Username": "3d19541048",
                "Password": "lazY2k"
            }
        }
        // console.log('options',options)
        request.post(
            options
            , function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                // console.log('httpResponse',httpResponse)
                console.log('GetPaymentDetails  CCC',jsonBody)
                staticMethods.sendApiPost(req,'/v1/orders/add-payment-details',{paymentDetails : JSON.stringify(jsonBody),OrderID : jsonBody.OrderID, paymentStatus : 1}, function (data) {
                    console.log('data  ZZZZZZZZ',data)
                    if(!data.error){
                        var options = {
                            url: 'https://servicestest.ameriabank.am/VPOS/api/VPOS/ConfirmPayment',
                            headers: {
                                'Accept': 'application/json',
                            },
                            form : {
                                "PaymentID": PaymentID,
                                "Username": "3d19541048",
                                "Password": "lazY2k"
                            }
                        }
                        console.log('options  CCC',options)
                        request.post(
                            options
                            , function (err, httpResponse, body) {
                                    var jsonBody = JSON.parse(body);
                                    // console.log('httpResponse',httpResponse)
                                    console.log('ConfirmPayment  CCC',jsonBody)
                                    res.render('orders/success',{
                                        siteData: req.siteData
                                    })
                            }
                        )
                        res.redirect('/orders/success')
                    }else{
                        res.redirect('/orders/failed')
                    }
                });
            }
        )
    }else{
        var jsonBody = {
            resposneCode : resposneCode,
            PaymentID : PaymentID,
            opaque : opaque,
            description : description
        }

        staticMethods.sendApiPost(req,'/v1/orders/add-payment-details',{paymentDetails : JSON.stringify(jsonBody),OrderID : orderID,paymentStatus : 0}, function (data) {
            console.log('........... resposneCode is not "00"')
            res.redirect('/orders/failed')
        });
    }
    // res.redirect('/orders/success')
});

router.get('/success/:public_id', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/orders/get-order-by-public-id',{public_id : req.params.public_id,langId : req.siteData.lang}, function (data) {
        console.log('data.data.orderProducts',typeof data.data.orderProducts)
        console.log('data.data.orderDetaling',typeof data.data.orderDetaling)
        if(data.data.orderProducts.length == 0){
            res.render('default/not_found',{
                siteData: req.siteData
            })
        }else{
            res.render('orders/cash_success_page',{
                siteData: req.siteData,
                orderProducts:data.data.orderProducts,
                orderInfo:data.data.orderDetaling,
                public_id:req.params.public_id
            })
        }

    });
});
router.get('/failed', function (req, res, next) {
    res.render('orders/failed',{
        siteData: req.siteData
    })
});

router.get('/cash', function (req, res, next) {
    res.render('orders/cash_success_page',{
        siteData: req.siteData
    })
});


module.exports = router;


