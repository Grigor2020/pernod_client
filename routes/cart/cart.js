var express = require('express');
var router = express.Router();
var statics = require('../../static');
var staticMethods = require('../../model/staticMethods');



router.get('/', function (req, res, next) {
    // console.log('cartttttttttttt',req.siteData.cart)
    // console.log('siteData.cart.length',req.siteData.cart.length)
    res.render('cart/cart',
        {
            siteData: req.siteData
        });
});

router.post('/add', function (req, res, next) {
    console.log('req.siteData.lang',req.siteData.lang)
    staticMethods.sendApiPost(req,'/v1/cart/add',{productId:req.body.productId,quantity:req.body.quantity, lang_id:req.siteData.lang}, function (data) {
        console.log('data',data)
        res.json(data)
    })
});


router.post('/remove', function (req, res, next) {
    // console.log('req.body',req.body)
    staticMethods.sendApiPost(req,'/v1/cart/remove',{productId:req.body.productId,lang : req.siteData.lang}, function (data) {
        // console.log('data',data)
        res.json(data)
    })
});

router.post('/remove-all', function (req, res, next) {
    // console.log('req.body',req.body)
    staticMethods.sendApiPost(req,'/v1/cart/remove-all',{productId:req.body.productId,lang : req.siteData.lang}, function (data) {
        // console.log('data',data)
        res.json(data)
    })
});


// router.post('/update-count', function (req, res, next) {
//     // console.log('req.body',req.body)
//     staticMethods.sendApiPost(req,'/v1/cart/update-quantity',{productId:req.body.productId,quantity:req.body.quantity}, function (data) {
//         res.json(data)
//     })
// });

module.exports = router;
