var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');


router.get('/', function (req, res, next) {
    var page = 1;
    if(typeof req.query.page !== "undefined")
        page = req.query.page;
    staticMethods.sendApiPost(req,'/v1/news/all',{langId : req.siteData.lang, page : page}, function (data) {
        if(!data.error){
            console.log('::::::::::::data.data.pagesCount',data.data.pagesCount)
            console.log('::::::::::::page',page)
            res.render('news/news',
                {
                    siteData: req.siteData,
                    data : data.data.items,
                    pagesCount : data.data.pagesCount,
                    thisPage : page
                });
        }
    })
});

router.get('/item/:slug', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/news/item',{langId : req.siteData.lang, slug : req.params.slug}, function (data) {
        if (!data.error) {
            res.render('news/newsItem',
                {
                    siteData: req.siteData,
                    data : data.data[0]
                });
        }else{
            res.redirect('/news?page=1')
        }
    });
});

module.exports = router;
