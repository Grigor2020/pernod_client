var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');



router.get('/verify-action/:email', function (req, res, next) {
    // console.log('req.params.email',req.params.email)
    res.render('auth/verify_email',
        {
            siteData: req.siteData,
            email : req.params.email
        });
});

router.get('/verify', function (req, res, next) {
    // console.log('req.query',req.query)
    if(typeof req.query.email !== "undefined" && req.query.email !== ""){
        staticMethods.sendApiPost(req,'/v1/auth/email/verify',{email:req.query.email}, function (data) {
            res.render('auth/verify_check',
                {
                    siteData: req.siteData
                });
        })
    }else{
        res.redirect('/')
    }

});

router.get('/authentication/:token', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/auth/email/verify-token',{token:req.params.token}, function (data) {
        if(!data.error){
            res.render('auth/verify_email_success',
                {
                    siteData: req.siteData
                });
        }else{
            res.render('auth/verify_email_failed',
                {
                    siteData: req.siteData
                });
        }

    })
});
router.get('/change/:token', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/auth/email/change/verify-token',{token:req.params.token}, function (data) {
        res.clearCookie('void');
        res.redirect('/login')
        // if(!data.error){
        //     res.render('auth/verify_email_success',
        //         {
        //             siteData: req.siteData
        //         });
        // }else{
        //     res.render('auth/verify_email_failed',
        //         {
        //             siteData: req.siteData
        //         });
        // }

    })
});



module.exports = router;
