var express = require('express');
var router = express.Router();
const request = require('request');
var static = require('../../static');
var staticMethods = require('../../model/staticMethods');


router.post('/check-data', function(req, res, next) {
    if(req.body.username.length > 0 && req.body.email.length > 0 ){
        let username = req.body.username;
        let email = req.body.email;
        const formData = {
            "a_username" :  req.body.username,
            "a_email" :  req.body.email
        };
        request.post(
            {
                url: static.API_URL+'/auth/check-info',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : static.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    res.json({error:false})
                }else{
                    res.json({error:true, msg : jsonBody.params})
                }
            }
        )
    }else{
        res.json({error:true,msg:'missing data'})
    }
});
router.post('/sign-up', function(req, res, next) {
    var bDate = staticMethods.underAgeValidate(req.body.bDate);
    if(bDate) {
        if ( req.body.name.length > 0 &&
            req.body.lastName.length > 0 &&
            req.body.bDate.length > 0 &&
            req.body.email.length > 0 &&
            req.body.phone.length > 0 &&
            req.body.password.length > 0 &&
            req.body.repassword.length > 0
        ){
            const formData = {
                "name" :req.body.name,
                "last_name" :  req.body.lastName,
                "email" :  req.body.email,
                "birth_date" :  req.body.bDate,
                "phone" :  req.body.phone,
                "password" :  req.body.password,
                "re_password" :  req.body.repassword
            };
            staticMethods.sendApiPost(req,'/v1/auth/sign-up',formData, function (data) {
                res.json(data)
                // console.log('/v1/auth/sign-up data',data)
                // if(!data.error){
                    // res.cookie('void', data.data.user.token,{expires: new Date(Date.now() + 900000000),path: '/'});
                    // res.json({error:false})
                    // res.end();
                    // res.json({error:true,msg:"0014" })
                // }else{
                //     res.json(data)
                //     res.end();
                // }
            })
        }else{
            res.json({error:true,msg : 'missing data'})
        }
    }else{
        res.json({error:true,msg : 'under 18'})
    }
});

router.post('/sign-in', function(req, res, next) {
    // console.log('req.body',req.body)
    const formData = {
        "email" :req.body.email,
        "password" :  req.body.password
    };
    staticMethods.sendApiPost(req,'/v1/auth/sign-in',formData, function (data) {
        if(!data.error){
            // console.log('data.data',data.data)
            if(data.data.user.email_verify === '0'){
                res.json({error:true,msg:"0014" })
                // res.redirect('/verify-email/verify-action/'+req.body.email+'');
            } else {
                res.cookie('void', data.data.user.token,{expires: new Date(Date.now() + 900000000),path: '/'});
                res.json({error:false})
            }
            res.end();
        }else{
            res.json(data)
            res.end();
        }
    })
});

router.get('/log-out', function(req, res, next) {
    res.clearCookie('void');
    res.redirect('/');
    res.end();

});

module.exports = router;
