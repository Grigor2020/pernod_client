var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');



router.get('/', function (req, res, next) {
    res.render('auth/reset_password',
        {
            siteData: req.siteData
        });
});
router.get('/verify/:token', function (req, res, next) {
    console.log('......',req.params.token)
    staticMethods.sendApiPost(req,'/v1/auth/forgot/check-token',{token : req.params.token}, function (data) {
        if(!data.error){
            console.log('...................................data',data)
            res.render('auth/new_password_resets',
                {
                    siteData: req.siteData
                });
        }
    })
});
router.post('/send-email', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/auth/forgot/check-email',{email : req.body.email}, function (data) {
        res.json({error:false})
    })
});
router.post('/new-passwords', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/auth/forgot/update-password',{token : req.body.token,password : req.body.pass,repassword : req.body.repass}, function (data) {
        res.json({error:false})
    })
});
module.exports = router;
