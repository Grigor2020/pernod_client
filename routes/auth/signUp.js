var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');



router.get('/', function (req, res, next) {
    if(typeof req.siteData.userInfo === "undefined") {
        res.render('auth/sign_up',
            {
                siteData: req.siteData
            });
    }else{
        res.redirect('/')
    }
});

module.exports = router;
