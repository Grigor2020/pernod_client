var express = require('express');
var router = express.Router();
var staticMethods = require('../../model/staticMethods');

router.get('/', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/about',{lang_id : req.siteData.lang}, (data)=>{
        res.render('about/about',
            {
                text : data.data[0].text,
                siteData: req.siteData
            });
    });
});

module.exports = router;
