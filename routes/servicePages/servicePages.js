var express = require('express');
var router = express.Router();
var staticMethods = require('../../model/staticMethods');


router.get('/return-and-refund-policy', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/service-pages/refPolicy',{lang_id : req.siteData.lang}, (data)=>{
        res.render('servicePages/refPolicy',
            {
                siteData: req.siteData,
                text : data.data[0].text
            });
    });
});

router.get('/terms', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/service-pages/terms',{lang_id : req.siteData.lang}, (data)=>{
        res.render('servicePages/terms',
            {
                siteData: req.siteData,
                text : data.data[0].text
            });
    });
});

router.get('/cookies', function (req, res, next) {
    console.log('req.siteData.lang',req.siteData.lang)
    staticMethods.sendApiPost(req,'/v1/service-pages/cookies',{lang_id : req.siteData.lang}, (data)=>{
        console.log('data',data)
        res.render('servicePages/cookies',
            {
                siteData: req.siteData,
                text : data.data[0].text
            });
    });
});
router.get('/privacy-policy', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/service-pages/privacy-policy',{lang_id : req.siteData.lang}, (data)=>{
        res.render('servicePages/privacyPolicy',
            {
                siteData: req.siteData,
                text : data.data[0].text
            });
    });
});
router.get('/faq', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/service-pages/faq',{lang_id : req.siteData.lang}, (data)=>{
        console.log('...............data',data)
        res.render('servicePages/faq',
            {
                siteData: req.siteData,
                faq : data.data
            });
    });
});

router.get('/faq', function (req, res, next) {
    res.render('servicePages/faq',
        {
            siteData: req.siteData
        });
});


module.exports = router;
