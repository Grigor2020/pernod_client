var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');

router.get('/', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/checkout/start', {lang_id:req.siteData.lang}, function (data) {
        console.log('data.data.cart',data.data.cart.items)
        if(!data.error){
            if(data.data.cart.items.length > 0) {
                res.render('checkout/checkout',
                    {
                        siteData: req.siteData,
                        countries: data.data.countries,
                        cart: data.data.cart,
                        addresses : data.data.addresses
                    });
            }else{
                res.redirect('/cart')
            }
        }else{
            res.redirect('/')
        }
    })
});
router.post('/get-states', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/checkout/get-states', {lang_id:req.siteData.lang,countryId : req.body.countryId}, function (data) {
        if(!data.error){
            res.json({error:false,data : {states:data.data.states} })
        }else{
            res.redirect('/')
        }
    })

});
router.post('/get-cities', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/checkout/get-cities', {lang_id:req.siteData.lang,stateId : req.body.stateId}, function (data) {
        if(!data.error){
            res.json({error:false,data : {cities:data.data.cities} })
        }else{
            res.redirect('/')
        }
    })

});



module.exports = router;


