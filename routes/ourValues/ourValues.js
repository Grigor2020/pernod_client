var express = require('express');
var router = express.Router();


router.get('/', function (req, res, next) {
    res.render('ourValues/our_values',
        {
            siteData: req.siteData
        });
});

module.exports = router;
