var express = require('express');
var router = express.Router();


router.get('/', function (req, res, next) {
    res.render('socialResponsibility/social_responsibility',
        {
            siteData: req.siteData
        });
});

module.exports = router;
