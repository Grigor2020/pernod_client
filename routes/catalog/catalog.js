var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');


router.get('/', function (req, res, next) {

    // if
    // (
    //     typeof req.query.cat === "undefined" ||
    //     typeof req.query.brand === "undefined" ||
    //     typeof req.query.filters === "undefined" ||
    //     typeof req.query.price === "undefined"
    // )
    // {
    //     console.log('11111111111111111111')
    //     var page = 1;
    //     if(typeof req.query.page !== "undefined"){
    //         page = req.query.page
    //     }
    //     var options = {
    //         url: statics.API_URL + '/v1/catalog/all/',
    //         headers: {
    //             'Accept': 'application/json',
    //             'authorization': statics.API_AUTH,
    //         },
    //         form : {
    //             langId : req.siteData.lang,
    //             page : page
    //         }
    //     }
    //     if(typeof req.siteData.userInfo !== "undefined" ){
    //         options.headers.void = req.siteData.userInfo.token
    //     }
    //     if(typeof req.siteData.guestInfo !== "undefined" ){
    //         options.headers.guest = req.siteData.guestInfo
    //     }
    //     // console.log('options',options)
    //     request.post(
    //         options
    //         , function (err, httpResponse, body) {
    //             var jsonBody = JSON.parse(body);
    //
    //             if(!jsonBody.error){
    //                 console.log('jsonBody',jsonBody)
    //                 res.render('catalog/catalog',
    //                     {
    //                         thisPage : parseInt(req.query.page),
    //                         priceRange : jsonBody.data.priceRange,
    //                         pagesCount : jsonBody.data.pagesCount,
    //                         // pagesCount : 5,
    //                         catalog : jsonBody.data.catalog,
    //                         filters : jsonBody.data.filters,
    //                         siteData: req.siteData,
    //
    //                     });
    //             }
    //         }
    //     )
    // }else{
    //     console.log('2222222222222222222')
    //     var prices = req.query.price.split(',')
    //     var cats = req.query.cat.split(',')
    //     var brands = req.query.brand.split(',')
    //     var allFilters = req.query.filters.split(',')
    //     var sendData = {
    //         "price": {
    //             "min":prices[0],
    //             "max":prices[1]
    //         },
    //         "cat":cats.length === 1 && cats[0] === '' ? [] : cats,    // ["brandy-cognac-4","tequila-5"],
    //         "brand":brands.length === 1 && brands[0] === '' ? [] : brands,
    //         "filters":allFilters.length === 1 && allFilters[0] === '' ? [] : allFilters,
    //     }
    //     console.log('sendData',sendData)
    //     staticMethods.sendApiPost(req,'/v1/catalog/get-by-filters',{page : req.query.page,data : JSON.stringify(sendData)},(result)=>{
        staticMethods.sendApiPost(req,'/v1/catalog/get-all-filters',{langId : req.siteData.lang},(result)=>{
            var brands = req.siteData.brands.sort(dynamicSort("name"));
            // console.log('result',result)
            if(!result.error){
                res.render('catalog/catalog',
                    {
                        // thisPage : parseInt(req.query.page),
                        // priceRange : result.data.priceRange,
                        // pagesCount : result.data.pagesCount,
                        // // pagesCount : 5,
                        // catalog : result.data.products,
                        filters : result.data,
                        siteData: req.siteData,
                        categories: req.siteData.categories,
                        litrages: req.siteData.litrages,
                        brands:brands

                    });
            }else{
                res.json({error : true,data : []})
            }
        })
    // }

});
function dynamicSort(property) {
    var sortOrder = 1;

    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }

    return function (a,b) {
        if(sortOrder == -1){
            return b[property].localeCompare(a[property]);
        }else{
            return a[property].localeCompare(b[property]);
        }
    }
}
router.post('/by-filter', function (req, res, next) {
    console.log('2222222222222222222',req.body.data)
    staticMethods.sendApiPost(req,'/v1/catalog/get-by-filters',{page : req.query.page,data : req.body.data},(result)=>{
        console.log('result',result)
        if(!result.error){
            res.json(result)
        }else{
            res.json({error : true,data : []})
        }
    })
});

router.post('/all', function (req, res, next) {
    console.log('333333333333333333333')
    staticMethods.sendApiPost(req,'/v1/catalog/all',{page : req.body.page,langId : req.siteData.lang},(result)=>{
        if(!result.error){
            res.json(result)
        }else{
            res.json({error : true,data : []})
        }
    })
});


module.exports = router;


