var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');


router.get('/', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/catalog/get-special-offers',{langId: req.siteData.lang}, (data)=> {
        if (!data.error) {
            res.render('catalog/special_offers',
                {
                    siteData: req.siteData,
                    data : data.data
                });
        }
    })
});

module.exports = router;


