var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var staticMethods = require('../../model/staticMethods');

router.get('/', function (req, res, next) {
    staticMethods.sendApiPost(req,'/v1/product/search',{vals : req.query.keys}, (data)=>{
        res.render('search/search',{
            siteData : req.siteData,
            products : data.data,
            searchKeys : req.query.keys
        })
    })
});

module.exports = router;


