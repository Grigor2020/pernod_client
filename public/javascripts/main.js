function sendPostAjax(url,params,callback){
    $.ajax({
        type: "POST",
        url: url,
        data: {params: params},
        dataType: "json",
        success: function (result) {
           callback(result);
        },
    });
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
function deleteCookie(name) {
    document.cookie = name + "=" + "; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/";
}

function setCook(name,value){
    document.cookie = name + "=" +value;
}

function requestPost(url,body,callback, showSpin){
    showSpin = showSpin === false ? false : true;
    if(showSpin) $('#cover-spin').fadeIn(200);

    var oAjaxReq = new XMLHttpRequest();
    oAjaxReq.open("post", url, true);
    oAjaxReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    oAjaxReq.send(body);
    oAjaxReq.onreadystatechange = callback;
}
