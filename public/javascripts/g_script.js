var FILTRATION_OBJ = {
    page : 1,
    price : {
        min : 0,
        max : 0
    },
    cat : [],
    brand : [],
    litrage : [],
    filters : []
}

//Range
function setFilterUrl(){
    var filterByUrl = 'page=1&cat=&brand=&litrage=&filters=&price='+FILTRATION_OBJ.price.min+','+FILTRATION_OBJ.price.max+'';

    let sUrl = filterByUrl.split('&')
    var arr = [];
    for(let i = 0 ;  i <sUrl.length; i++ ){
        let splitedSect = sUrl[i].split('=')
        if(splitedSect[0] == 'page')
            continue
        if(splitedSect[0] == 'price'){
            splitedSect[1] = FILTRATION_OBJ.price.min+','+FILTRATION_OBJ.price.max
        }else{
            splitedSect[1] = FILTRATION_OBJ[splitedSect[0]].join(',')
        }

        var sectUrl = splitedSect.join('=')
        arr.push(sectUrl)
    }
    window.history.pushState(  "?page=1&"+arr.join('&'),'newUrl',"?page=1&"+arr.join('&'));
}

$(window).on('popstate', function (e) {
    var state = e.originalEvent.state;
    if (state !== null) {
        sendRequest(FILTRATION_OBJ)
    }else{
        var filterObjInStart = {
            page : 1,
            price : {
                min : 1,
                max : 10000000
            },
            cat : [],
            brand : [],
            litrage : [],
            filters : []
        }
        sendRequest(filterObjInStart)
    }
});


//
//
// $("#min_price,#max_price").on("paste keyup", function () {
//     $('#price-range-submit').show();
//
//     var min_price_range = parseInt($("#min_price").val());
//
//     var max_price_range = parseInt($("#max_price").val());
//
//     if (min_price_range == max_price_range) {
//
//         max_price_range = min_price_range + 1000;
//
//         $("#min_price").val(min_price_range);
//         $("#max_price").val(max_price_range);
//     }
//
//     $("#slider-range").slider({
//         values: [min_price_range, max_price_range]
//     });
// });


function detectPrices() {
    {
        $(function () {
            try {
                $("#min_price,#max_price").on('change', function () {

                    $('#price-range-submit').show();

                    var min_price_range = parseInt($("#min_price").val());

                    var max_price_range = parseInt($("#max_price").val());


                    console.log('min_price_range',min_price_range)
                    console.log('max_price_range',max_price_range)


                    if (min_price_range > max_price_range) {
                        $('#max_price').val(min_price_range);
                    }

                    $("#slider-range").slider({
                        values: [min_price_range, max_price_range]
                    });
                    FILTRATION_OBJ.price.min = min_price_range
                    FILTRATION_OBJ.price.max = max_price_range
                    sendRequest(FILTRATION_OBJ);
                    $('#cover-spin').fadeOut(200);
                });

                // let minSliderF = $('input#min_price').attr('min').split('');
                // minSliderF.length >= 4 ?
                //     minSliderF.splice(minSliderF.length - 3, 3, 0, 0, 0) : minSliderF;
                // minSliderF = parseInt(minSliderF.join(''));
                var minSliderF = parseInt($('input#min_price').attr('min'));
                var maxSliderF = parseInt($('input#max_price').attr('max'));
                FILTRATION_OBJ.price.min = minSliderF
                FILTRATION_OBJ.price.max = maxSliderF
                $("#slider-range").slider({
                    range: true,
                    orientation: "horizontal",
                    min: minSliderF,
                    max: maxSliderF,
                    values: [minSliderF, maxSliderF],
                    step: 5000,

                    slide: function (event, ui) {
                        if (ui.values[0] == ui.values[1]) {
                            return false;
                        }

                        $("#min_price").val(parseInt(ui.values[0]));
                        $("#max_price").val(parseInt(ui.values[1]));
                    },
                    change: function (event, ui) {
                        FILTRATION_OBJ.price.min = parseInt($("#min_price").val())
                        FILTRATION_OBJ.price.max = parseInt($("#max_price").val())
                        setFilterUrl()
                        sendRequest(FILTRATION_OBJ);
                    }
                });
                $("#min_price").val($("#slider-range").slider("values", 0));
                $("#max_price").val($("#slider-range").slider("values", 1));
                $('#cover-spin').fadeOut(200);
            } catch (e) {
                console.log(e)
            }
        });
    }
    // reOrderAllFilters($('.filter_col.filter_brand'));
}
//Range


$(document).on('change','.catalog_filter input[type="checkbox"]', function () {

// })
// $('.catalog_filter input[type="checkbox"]').on("change", function(e){
    // if($(this).is(':checked')) {
        const type = $(this).attr("data-type");
        const id = $(this).attr("id");
        switch (type) {
            case 'cat' :
                if(FILTRATION_OBJ.cat.indexOf(id) === -1){
                    FILTRATION_OBJ.cat.push(id)
                }else{
                    FILTRATION_OBJ.cat.splice(FILTRATION_OBJ.cat.indexOf(id),1)
                }
                break
            case 'brand' :
                if(FILTRATION_OBJ.brand.indexOf(id) === -1){
                    FILTRATION_OBJ.brand.push(id)
                }else{
                    FILTRATION_OBJ.brand.splice(FILTRATION_OBJ.brand.indexOf(id),1)
                }
                break
            case 'litrage' :
                if(FILTRATION_OBJ.litrage.indexOf(id) === -1){
                    FILTRATION_OBJ.litrage.push(id)
                }else{
                    FILTRATION_OBJ.litrage.splice(FILTRATION_OBJ.litrage.indexOf(id),1)
                }
                break
            case 'filter' :
                if(FILTRATION_OBJ.filters.indexOf(id) === -1){
                    FILTRATION_OBJ.filters.push(id)
                }else{
                    FILTRATION_OBJ.filters.splice(FILTRATION_OBJ.filters.indexOf(id),1)
                }
                break
        }

    // }
    setFilterUrl()
    sendRequest(FILTRATION_OBJ);
});

// function runSticky(){
//     try{
//         console.log('StickyStickyStickySticky');
//         var sticky = new Sticky('[data-sticky]', {});
//     }catch (e){
//         console.log(e)}
// }

function sendRequest(filtration){
    $('#cover-spin').fadeIn(200);
    requestPost('/catalog/by-filter','data='+JSON.stringify(filtration)+'', function () {
        if(this.readyState == 4){
            // var $filtersMain = $('#g_filters');

            var result = JSON.parse(this.responseText)
            // console.log('sendRequest FILTRATION_OBJ',FILTRATION_OBJ)
            // console.log('sendRequest result',result.data)

            FILTRATION_OBJ.price.min = result.data.priceRange.min_price
            FILTRATION_OBJ.price.max = result.data.priceRange.max_price
            // console.log('sendRequest FILTRATION_OBJ',FILTRATION_OBJ)
            var filters = result.data.filters;
            var products = result.data.products;

            setPrducts(products)
            detectAndCheckCheckboxesFromUrl(filters)
            // setPriceFiltration()
            $('#cover-spin').fadeOut(200);

            // $('.filter_col:not(.price_range_block)').each(function (e){
            //     reOrderAllFilters($(this));
            // })

        }
    })
}

function setPrducts(products) {
    var $productsMain = $('#g_products');
    $productsMain.html('');
    var productsContent = "";
    if(products.length > 0){
        for(let i = 0 ; i < products.length;i++){
            var description = '';
            if(products[i].description !== null)
                description = products[i].description
            var active = products[i].favId !== null ? 'active' : '';
            var priceBeforeSale = products[i].price_before_sale !== null ? `<del>${products[i].price_before_sale} <sup>֏</sup></del>` : ""
            var stockButton = products[i].in_stock == "0"?
                `<a href="javascript:;" data-open="#cart_popup_id" class="product_add_cart" data-id="${products[i].product_id}" ><span class="app_icon cart_icon"></span>ADD TO CART</a>`
                :`<span class="product_add_cart_hide">Out of stock</span>`;
            var stockPrice = products[i].in_stock == "0" ? `` : `<span class="product_box_price hide">Out of stock</span>`;


            productsContent += `<div class="product_col">
                                        <div class="product_box">
                                            <div class="product_box_content">
                                                <h3 class="secondary_title">${products[i].name}</h3>
                                                <div class="product_box_img">
                                                    <img src="${products[i].image}" alt="${products[i].name}">
                                                </div>
                                                <div class="product_box_description">${description}</div>
                                                <span class="product_box_price">${products[i].price} <sup>֏</sup>  ${priceBeforeSale}</span>
                                                ${stockPrice}                                       
                                            </div>
                                            <div class="product_box_content_back" style="background-image: url(${products[i].hoverImage})">
                                                <button type="button" class="favorites_toggle_btn ${active}" data-id="${products[i].product_id}">
                                                    <span>
                                                        <svg width="14" height="13" viewBox="0 0 14 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M6.64661 1.68689L7.00016 2.04044L7.35372 1.68689C7.96047 1.08013 8.934 0.5 10.0002 0.5C11.7191 0.5 13.1668 1.91151 13.1668 4C13.1668 6.13994 12.023 7.85077 10.6155 9.17787C9.36731 10.3547 7.96134 11.1845 7.06448 11.7138C7.04274 11.7267 7.02129 11.7393 7.00016 11.7518C6.97903 11.7393 6.95759 11.7267 6.93585 11.7138C6.03899 11.1845 4.63302 10.3547 3.38484 9.17787C1.97731 7.85077 0.833496 6.13994 0.833496 4C0.833496 1.91382 2.30443 0.5 4.00016 0.5C5.06632 0.5 6.03985 1.08013 6.64661 1.68689Z" stroke="#005092"/>
                                                        </svg>
                                                    </span>
                                                </button>
                                                <a href="/product?id=${products[i].slug}" class="product_box_bg"></a>
                                                <div class="product_price">${products[i].price} ֏</div>
                                                <a href="/product?id=${products[i].slug}"  class="product_box_more"><span class="app_icon more_icon"></span>MORE</a>
                                                ${stockButton}
                                            </div>
                                        </div>
                                    </div>`;
        }
    }else{
        productsContent = `<div class="col-12">
                                <div class="cart_popup_box catalog_empty">
                                    <div class="cart_popup_box_img">
                                        <img src="images/content/general/catalog_emtpy_icon.svg" alt="">
                                    </div>
                                    <p>We didn't find any results</p>
                                    <a href="/catalog?page=1" class="cart_popup_box_btn btn_outline">Rest results</a>
                                </div>
                            </div>`;
    }
    $productsMain.html(productsContent)

    // runSticky();
    // $('#cover-spin').fadeOut(200);
}

function detectAndCheckCheckboxesFromUrl(filterAvailable){
    var cats = []
    var brands = []
    var litrage = []
    var filters = []
    var prices = {};
    let fUrl = window.location.href.split('?')
    console.log('fUrl',fUrl)
    // if(FILTRATION_OBJ.brand.length === 0 && FILTRATION_OBJ.cat.length === 0 && FILTRATION_OBJ.filters.length === 0 ){
    //     return false
    // }else {
    if(fUrl.length > 1) {
        let sUrl = fUrl[1].split('&')
        for (let i = 0; i < sUrl.length; i++) {
            let urlSection = sUrl[i].split('=');
            if (urlSection[0] === 'page') {
                continue;
            } else {
                var infoBlock = urlSection[1].split(',')
                if (infoBlock.length === 1 && infoBlock[0] === '') {
                    continue
                } else {
                    switch (urlSection[0]) {
                        case 'cat' :
                            cats = infoBlock
                            FILTRATION_OBJ.cat = cats
                            break
                        case 'brand' :
                            brands = infoBlock
                            FILTRATION_OBJ.brand = brands
                            break
                        case 'litrage' :
                            litrage = infoBlock
                            FILTRATION_OBJ.litrage = litrage
                            break
                        case 'filters' :
                            filters = infoBlock
                            break
                        case 'price' :
                            FILTRATION_OBJ.price.min = infoBlock[0]
                            FILTRATION_OBJ.price.max = infoBlock[1]
                            prices.min = infoBlock[0]
                            prices.max = infoBlock[1]
                            break
                    }
                }
            }
        }
        var catBlocks = $('input[data-type="cat"]');
        for (var i = 0; i < catBlocks.length; i++) {
            var name = $(catBlocks[i]).attr('id');
            if (FILTRATION_OBJ.cat.indexOf(name) !== -1) {
                $(catBlocks[i]).prop("checked", true);
            } else {
                $(catBlocks[i]).prop("checked", false);
            }
        }
        var brandBlocks = $('input[data-type="brand"]');
        for (var i = 0; i < brandBlocks.length; i++) {
            var name = $(brandBlocks[i]).attr('id');
            if (filters.indexOf(name) !== -1 || FILTRATION_OBJ.brand.indexOf(name) !== -1) {
                $(brandBlocks[i]).prop("checked", true);
            } else {
                $(brandBlocks[i]).prop("checked", false);
            }
            // if (filterAvailable.indexOf(name) !== -1) {
            //     $(brandBlocks[i]).removeAttr("disabled");
            // } else {
            //     $(brandBlocks[i]).attr("disabled", true);
            // }
        }
        var litrageBlocks = $('input[data-type="litrage"]');

        for (var i = 0; i < litrageBlocks.length; i++) {
            var name = $(litrageBlocks[i]).attr('id');

            if (filters.indexOf(name) !== -1 || FILTRATION_OBJ.litrage.indexOf(name) !== -1) {
                $(litrageBlocks[i]).prop("checked", true);
            } else {
                $(litrageBlocks[i]).prop("checked", false);
            }
            // if (filterAvailable.indexOf(name) !== -1) {
            //     $(litrage[i]).removeAttr("disabled");
            // } else {
            //     $(litrage[i]).attr("disabled", true);
            // }
        }
        // var filterBlocks = $('input[data-type="filter"]');
        // for (var i = 0; i < filterBlocks.length; i++) {
        //     var name = $(filterBlocks[i]).attr('id');
        //     if (filters.indexOf(name) !== -1) {
        //         $(filterBlocks[i]).prop("checked", true);
        //     } else {
        //         $(filterBlocks[i]).prop("checked", false);
        //     }
        //     if (filterAvailable.indexOf(parseInt(name)) !== -1) {
        //         $(filterBlocks[i]).removeAttr("disabled");
        //     } else {
        //         $(filterBlocks[i]).attr("disabled", true);
        //     }
        // }
        // }
    }else{
        // console.log('qqqqqqqqqq')
        window.location.reload();
    }
}

function setPriceFiltration(){
    var content = "";

    content +=
        `
            <div class="price_range_col">
                <input disabled  type="number" min="${FILTRATION_OBJ.price.min}" max="${FILTRATION_OBJ.price.max} - 1000" oninput="validity.valid||(value='${FILTRATION_OBJ.price.min}');" id="min_price" class="price-range-field" />֏
            </div>
            <div class="price_range_col">
                <div class="price_line"></div>
            </div>
            <div class="price_range_col">
                <input disabled type="number" min="${FILTRATION_OBJ.price.min}" max="${FILTRATION_OBJ.price.max}" oninput="validity.valid||(value='${FILTRATION_OBJ.price.max}');" id="max_price" class="price-range-field" />֏
            </div>
        `;
    $('.price_range_inputs').html(content);
    detectPrices();
}

$(document).ready(function () {

    let fUrl = window.location.href.split('?')
    if(typeof fUrl[1] === "undefined"){
        requestPost('/catalog/all',"page=1", function () {
            if (this.readyState == 4) {
                var result = JSON.parse(this.responseText)
                // console.log('11 /all result',result)
                if(!result.error){
                    FILTRATION_OBJ.price.min = result.data.priceRange.min_price
                    FILTRATION_OBJ.price.max = result.data.priceRange.max_price
                    setPrducts(result.data.catalog)
                    setPriceFiltration();
                }
            }
        })
    } else {
        let sUrl = fUrl[1].split('&')
        if(sUrl.length === 1){
            var tUrl = sUrl[0].split('=');
            requestPost('/catalog/all',"page="+tUrl[1]+"", function () {
                if (this.readyState == 4) {
                    var result = JSON.parse(this.responseText)
                    // console.log('22 /all result',result)
                    if(!result.error){
                        FILTRATION_OBJ.price.min = result.data.priceRange.min_price
                        FILTRATION_OBJ.price.max = result.data.priceRange.max_price
                        setPrducts(result.data.catalog)
                        setPriceFiltration();
                    }
                }
            })
        } else {

            for (let i = 0; i < sUrl.length; i++) {
                let urlSection = sUrl[i].split('=');
                var infoBlock = urlSection[1].split(',')
                if (infoBlock.length === 1 && infoBlock[0] === '') {
                    continue
                } else {
                    // console.log(',,,,,,,,,,,,,,,,,,,infoBlock',infoBlock)
                    switch (urlSection[0]) {
                        case 'page' :
                            FILTRATION_OBJ.page = infoBlock
                            break
                        case 'cat' :
                            FILTRATION_OBJ.cat = infoBlock
                            break
                        case 'brand' :
                            FILTRATION_OBJ.brand = infoBlock
                            break
                        case 'litrage' :
                            litrage = infoBlock
                            FILTRATION_OBJ.litrage = litrage
                            break
                        case 'filters' :
                            FILTRATION_OBJ.filters = infoBlock
                            break
                        case 'price' :
                            FILTRATION_OBJ.price.min = infoBlock[0]
                            FILTRATION_OBJ.price.max = infoBlock[1]
                            break
                    }
                }
            }
            console.log('FILTRATION_OBJ',FILTRATION_OBJ)
            sendRequest(FILTRATION_OBJ);
            setPriceFiltration();
        }
    }
})


// function reOrderAllFilters(parent){
//     var orderCount = 1;
//
//     if( parent.find('.catalog_filter_title').length == 1){
//         parent.find('.catalog_filter_title').css({
//             order: orderCount
//         });
//         orderCount++;
//     }
//     if(parent.find('.input_checkbox').length > 0){
//         var chekboxes = parent.find('.input_checkbox');
//         var arrayDisabled = [], k = 0 ;
//         chekboxes.each(function (e){
//             if(!$(this).children('input').is(':disabled')){
//                 $(this).css({
//                     order: orderCount,
//                     display: 'block'
//                 });
//                 $(this).removeClass('hide');
//                 $(this).slideDown(0);
//                 if(parent.hasClass('filter_brand') && orderCount > 6){
//                     $(this).slideUp(0).addClass('hide');
//                 }
//                 orderCount++;
//             }else{
//                 arrayDisabled[k++] = $(this);
//             }
//         });
//
//         for(var i in arrayDisabled){
//             arrayDisabled[i].css({
//                 order: orderCount,
//                 display: 'block'
//             });
//             arrayDisabled[i].removeClass('hide');
//             arrayDisabled[i].slideDown(0);
//             if(parent.hasClass('filter_brand') && orderCount > 6){
//                 arrayDisabled[i].slideUp(0).addClass('hide');
//             }
//             orderCount++;
//         }
//
//         // {
//         //     if(chekboxes.length <= 5){
//         //         try{parent.find('a.btn_show_more').css({'display':'none'});}catch (e){}
//         //     }else{
//         //         chekboxes.each(function (j) {
//         //             if(chekboxes.find('input').)
//         //                 $(this).slideUp(0).addClass('hide');
//         //         });
//         //     }
//         // }
//     }
//
//     if( parent.find('.btn_show_more').length == 1){
//         parent.find('.btn_show_more').css({
//             order: orderCount
//         });
//         if(parent.find('.btn_show_more').hasClass('open')){
//             parent.find('.btn_show_more').removeClass('open');
//             var dataText = parent.find('.btn_show_more').data('placeholder');
//             parent.find('.btn_show_more').data('placeholder',parent.find('.btn_show_more').text());
//             parent.find('.btn_show_more').text(dataText);
//         }
//     }
// }
// $('.filter_col:not(.price_range_block)').each(function (e){
//     reOrderAllFilters($(this));
// })
