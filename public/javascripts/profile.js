jQuery(document).ready(function ($) {
    // var account_statusArray = {
    //     loyalty_info : [
    //         {
    //             id : 1,
    //             name : 'STANDART',
    //             start_from : 0, // AMD
    //             color_1 : '#005092',
    //             color_2 : '#4EA6EE'
    //         },
    //         {
    //             id : 2,
    //             name : 'SILVER',
    //             start_from : 200000, // AMD
    //             color_1 : '#858585',
    //             color_2 : '#DEDEDE'
    //         },
    //         {
    //             id : 3,
    //             name : 'PLATINIUM',
    //             start_from : 600000, // AMD
    //             color_1 : '#A0B2C6',
    //             color_2 : '#B7CFE9'
    //         },
    //         {
    //             id : 4,
    //             name : 'GOLD',
    //             start_from : 900000, // AMD
    //             color_1 : '#D4AF37',
    //             color_2 : '#FDCF3B'
    //         }
    //     ],
    //     user_loyalty : {
    //         spent_money : 610000,
    //         loyality_status_id : 3
    //     }
    // }

    if (typeof data !== "undefined") {
        var account_statusArray = JSON.parse(data)
        console.log('account_statusArray',account_statusArray)
        try {
            function drowStatusProgress() {
                try {
                    let progressItems = account_statusArray.loyalty_info;
                    let progressItemsCount = account_statusArray.loyalty_info.length;
                    let maxStartForm = account_statusArray.loyalty_info[progressItemsCount - 1].start_from;
                    let userSpentMoney = account_statusArray.user_loyalty.spent_money;
                    let user = account_statusArray.user_loyalty;
                    let bar_container = $('.status_progress_bar');
                    let prosentForBarItem = 100 / (progressItemsCount - 1);
                    let startProsent = 0;
                    let flag = false;
                    let arrayPrices = [], t = 0;
                    bar_container.empty();

                    if ($(window).width() > 767) {

                        for (let i of account_statusArray.loyalty_info) {
                            let newItem;
                            if (!flag) {
                                if (account_statusArray.user_loyalty.loyality_status_id >= i.id) {
                                    newItem = $(`<div class="progress_bar_item active" style="left: ${startProsent}%">
                                            <div class="progress_bar_price" style="color: ${i.color_1}">${i.start_from} <span>֏</span></div>
                                            <span data-val=${i.start_from} class="progress_bar_icon" style="background: linear-gradient(180deg, ${i.color_1} 0%, ${i.color_2} 100%)"></span>
                                            <div class="progress_bar_title" style="color: ${i.color_1}">${i.name}</div>
                                        </div>`);
                                } else {
                                    newItem = $(`<div  class="progress_bar_item" style="left: ${startProsent}%">
                                            <div class="progress_bar_price">i.start_from <span>֏</span></div>
                                            <span data-val=${i.start_from} class="progress_bar_icon"></span>
                                            <div class="progress_bar_title">${i.name}</div>
                                        </div>`);
                                }
                                flag = true;
                                arrayPrices[t++] = {k: i.id, val: i.start_from};
                            } else {
                                if (account_statusArray.user_loyalty.loyality_status_id >= i.id) {
                                    newItem = $(`<div  class="progress_bar_item active" style="left: ${startProsent}%;transform: translateX(-${startProsent}%)">
                                            <div class="progress_bar_price" style="color: ${i.color_1}">${i.start_from} <span>֏</span></div>
                                            <span data-val=${i.start_from} class="progress_bar_icon" style="background: linear-gradient(180deg, ${i.color_1} 0%, ${i.color_2} 100%)"></span>
                                            <div class="progress_bar_title" style="color: ${i.color_1}">${i.name}</div>
                                        </div>`);
                                } else {
                                    newItem = $(`<div class="progress_bar_item" style="left: ${startProsent}%;transform: translateX(-${startProsent}%)">
                                            <div class="progress_bar_price">${i.start_from} <span>֏</span></div>
                                            <span data-val=${i.start_from}  class="progress_bar_icon"></span>
                                            <div class="progress_bar_title">${i.name}</div>
                                        </div>`);
                                }
                                arrayPrices[t++] = {k: i.id, val: i.start_from};
                            }

                            startProsent += prosentForBarItem;
                            bar_container.append(newItem);
                        }

                        let w1 = bar_container.find('.progress_bar_item').eq(0).width() / 2;
                        let w2 = bar_container.find('.progress_bar_item').eq(progressItemsCount - 1).width() / 2;
                        let wA = w1 + w2;
                        let topP = bar_container.find('.progress_bar_item').eq(0).find('.progress_bar_icon').position().top;


                        let middleLine = [];
                        for (let r = 0; r < arrayPrices.length; r++) {
                            if (arrayPrices[r].val >= userSpentMoney && userSpentMoney > 0) {
                                middleLine[0] = arrayPrices[r - 1].val;
                                middleLine[1] = arrayPrices[r].val;
                                break;
                            }else if(userSpentMoney <= 0){
                                middleLine[0] = 0;
                                middleLine[1] = arrayPrices[r].val;
                                break;
                            }else if(arrayPrices[arrayPrices.length-1].val <= userSpentMoney){
                                middleLine[0] = null;
                                middleLine[1] = null;
                                break;
                            }
                        }
                        var startLienF = $('[data-val]').eq(0).offset().left;

                        let endLienF;
                        let midNoLine;
                        $('[data-val]').each(function (w) {
                            if ($(this).attr('data-val') == middleLine[1]) {
                                endLienF = $(this).offset().left;
                            }
                            if ($(this).attr('data-val') == middleLine[0]) {
                                midNoLine = $(this).offset().left;
                            }
                        });
                        let bigLine = isNaN(endLienF - startLienF) ? 100 : endLienF - startLienF;
                        let smallLine = isNaN((endLienF - midNoLine) * ((middleLine[1] - userSpentMoney) / (middleLine[1] - middleLine[0]) * 100) / 100) ?
                            0 : (endLienF - midNoLine) * ((middleLine[1] - userSpentMoney) / (middleLine[1] - middleLine[0]) * 100) / 100;

                        let widthW = isNaN(endLienF - startLienF) && isNaN((endLienF - midNoLine) * ((middleLine[1] - userSpentMoney) / (middleLine[1] - middleLine[0]) * 100) / 100) ? (bigLine - smallLine)+'%' : (bigLine - smallLine)+'px'

                        let user_prog_bar = $(`<div class="progress_bar" >
                            <span class="user_progress" style='width: ${widthW}'></span>
                        </div>`);

                        bar_container.append(user_prog_bar);
                        bar_container.find('.progress_bar').css({
                            width: `calc(100% - ${wA}px)`,
                            left: `${w1}px`,
                            top: `${topP + 12 + 24 / 2}px`
                        });
                    } else {
                        let middleArray = [], p = 0;

                        for (let i = 0; i < progressItems.length; i++) {
                            if (user.loyality_status_id == progressItems[i].id) {
                                middleArray[p++] = progressItems[i];
                                if (i !== progressItems.length - 1) {
                                    middleArray[p] = progressItems[++i];
                                }
                                break;
                            }
                        }
                        let prosentForBarItem = 100;

                        for (let i of middleArray) {
                            let newItem;
                            if (!flag) {
                                if (account_statusArray.user_loyalty.loyality_status_id >= i.id) {
                                    newItem = $(`<div class="progress_bar_item active" style="left: ${startProsent}%">
                                            <div class="progress_bar_price" style="color: ${i.color_1}">${i.start_from} <span>֏</span></div>
                                            <span data-val=${i.start_from} class="progress_bar_icon" style="background: linear-gradient(180deg, ${i.color_1} 0%, ${i.color_2} 100%)"></span>
                                            <div class="progress_bar_title" style="color: ${i.color_1}">${i.name}</div>
                                        </div>`);
                                } else {
                                    newItem = $(`<div  class="progress_bar_item" style="left: ${startProsent}%">
                                            <div class="progress_bar_price">i.start_from <span>֏</span></div>
                                            <span data-val=${i.start_from} class="progress_bar_icon"></span>
                                            <div class="progress_bar_title">${i.name}</div>
                                        </div>`);
                                }
                                flag = true;
                                arrayPrices[t++] = {k: i.id, val: i.start_from};
                            } else {
                                if (account_statusArray.user_loyalty.loyality_status_id >= i.id) {
                                    newItem = $(`<div  class="progress_bar_item active" style="left:0">
                                            <div class="progress_bar_price" style="color: ${i.color_1}">${i.start_from} <span>֏</span></div>
                                            <span data-val=${i.start_from} class="progress_bar_icon" style="background: linear-gradient(180deg, ${i.color_1} 0%, ${i.color_2} 100%)"></span>
                                            <div class="progress_bar_title" style="color: ${i.color_1}">${i.name}</div>
                                        </div>`);
                                } else {
                                    newItem = $(`<div class="progress_bar_item" style="right:0;text-align: right">
                                            <div class="progress_bar_price" style="text-align: right">${i.start_from} <span>֏</span></div>
                                            <span data-val=${i.start_from}  class="progress_bar_icon" style="margin-left: auto"></span>
                                            <div class="progress_bar_title" style="text-align: right">${i.name}</div>
                                        </div>`);
                                }
                                arrayPrices[t++] = {k: i.id, val: i.start_from};
                            }

                            startProsent += prosentForBarItem;
                            bar_container.append(newItem);
                        }

                        let topP = bar_container.find('.progress_bar_item').eq(0).find('.progress_bar_icon').position().top;


                        let middleLine = arrayPrices.length < 2 ? 0: arrayPrices[1].val - arrayPrices[0].val;
                        let midduserSpentMoney = isNaN(userSpentMoney - arrayPrices[0].val) ? 0 : userSpentMoney - arrayPrices[0].val;

                        let smallLine = isNaN((midduserSpentMoney / middleLine) * 100) || (midduserSpentMoney / middleLine) * 100 <= 0 ?
                            0 : (midduserSpentMoney / middleLine) * 100;

                        let user_prog_bar = $(`<div class="progress_bar" >
                            <span class="user_progress" style='width: ${smallLine}%'></span>
                        </div>`);

                        bar_container.append(user_prog_bar);
                        bar_container.find('.progress_bar').css({
                            top: `${topP + 12 + 24 / 2}px`
                        });

                        console.log(bar_container.find('.progress_bar_item').length)
                        if(bar_container.find('.progress_bar_item').length == 1){
                            bar_container.find('.progress_bar_item').css({left: `calc(50% - ${bar_container.find('.progress_bar_item').width()/2}px)`}).addClass('center')
                            bar_container.find('.progress_bar .user_progress').css({width: '100%'})
                        }
                    }
                } catch (e) {
                    console.log(e)
                }
            }

            $(window).resize(function (e) {
                drowStatusProgress();
            });
            drowStatusProgress();
        } catch (e) {}
    }

    $(document).on('click', '#prindOrder', function () {
        var printContents = document.getElementById('printableArea').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    })
    // $(document).on('click', '#generateInvoice', function () {
        // requestPost('/profile/order/get-invoice', 'publicId='+$(this).attr('data-pid')+'', function () {
        //     if(this.readyState == 4) {
        //         let result = JSON.parse(this.responseText);
        //         console.log('result',result)
        //     }
        // })
    // })
});